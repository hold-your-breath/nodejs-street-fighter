const { HTTP_REQUEST_ERROR } = require('../helpers/constants');

const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.data) {
        return res.status(200).json(res.data);
    }
    if (res.err) {
        console.log(HTTP_REQUEST_ERROR);
        const {statusCode} = res.err;
        return res.status(statusCode).json({ error: true, message: res.err.message });
    }
    next();
}

exports.responseMiddleware = responseMiddleware;